from django import template
from car.models import publishedCar ,publishedCarContents_Text,publishedCarDetails_Text

register =template.Library()

#عشان اجيب العربيات اللي ضافها شخص معين
@register.filter
def user_car(person):
    cars =publishedCar.objects.filter(customerId =person)
    return cars

#لو عايزه اجيب صفة محددة من صفات العربيه اللي موجوده في الdetails
@register.filter
def used_car_detail_txt(car ,detail):

    value =''
    try:
        d = publishedCarDetails_Text.objects.get(name=detail)
        result = publishedCarContents_Text.objects.filter(publishedCarId=car).get(publishedCarDetailsId=d)
        value =result.value
    except :
        pass

    return value


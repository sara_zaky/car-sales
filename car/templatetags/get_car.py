from django import template
from car.models import publishedCarContents_Bool,publishedCarContents_Text,carContents_Bool,carContents_Text

register=template.Library()

@register.filter
def getUsedText(value):

   cars=publishedCarContents_Text.objects.filter(publishedCarId=value)
   return cars

@register.filter
def getUsedBool(value):
    cars = publishedCarContents_Bool.objects.filter(publishedCarId =value)
    return cars

@register.filter
def getNewText(value):
    cars = carContents_Text.objects.filter(carId=value)
    return cars

@register.filter
def getNewBool(value):
    cars = carContents_Bool.objects.filter(carId=value)
    return cars
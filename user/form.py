from django import forms
from .models import profile ,governorate ,region
from django.contrib.auth.models import User


class UserUpdateForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['username']


class ProfileUpdateForm(forms.ModelForm):

    class Meta:
        model = profile
        fields = [ 'الاسم', 'المنطقة','رقم_الهاتف','صورة_شخصية']


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['المنطقة'].queryset = region.objects.none()

        if 'المحافظة' in self.data:
            try:
                gov_id = int(self.data.get('المحافظة'))
                self.fields['المنطقة'].queryset = region.objects.filter(governorateId=gov_id)
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.المنطقة:
            self.fields['المنطقة'].queryset = self.instance.المنطقة.governorateId.region_set.order_by('المنطقة')


class governorate_form(forms.ModelForm):
    المحافظة = forms.ModelChoiceField(queryset= governorate.objects.all())
    class Meta:
        model =governorate
        fields =['المحافظة',]

    def __init__(self, *args, **kwargs):
        super(governorate_form, self).__init__(*args, **kwargs)

        if self.instance.profile.المنطقة:
            self.initial['المحافظة'] = self.instance.profile.المنطقة.governorateId.id


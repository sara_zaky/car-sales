from django.db import models
from django.contrib.auth.models import User

class company(models.Model):
    الماركة = models.CharField(max_length=20)

    def __str__(self):
        return self.الماركة

class model(models.Model):
    companyId =models.ForeignKey(company ,on_delete=models.CASCADE)
    الموديل = models.CharField(max_length=20)

    def __str__(self):
        return self.الموديل

class newCar(models.Model):
    modelId = models.ForeignKey(model ,on_delete=models.CASCADE)
    الفئة =models.CharField(max_length=20)
    السنة = models.IntegerField()
    السعر = models.FloatField(max_length=10)

class image(models.Model):
    carID =models.ForeignKey(newCar ,on_delete=models.CASCADE)
    صورة = models.ImageField(default='default.jpg', upload_to='car_img')

class carDetails_Text(models.Model):
    name =models.CharField(max_length=20 , primary_key=True)

class carDetails_Bool(models.Model):
    name =models.CharField(max_length=20 , primary_key=True)


class carContents_Text(models.Model):
    class Meta:
        unique_together =(('carId', 'carDetailsId'),)

    carId =models.ForeignKey(newCar,on_delete=models.CASCADE )
    carDetailsId =models.ForeignKey(carDetails_Text,on_delete=models.CASCADE )
    value =models.CharField(max_length=50)

class carContents_Bool(models.Model):
    class Meta:
        unique_together =(('carId', 'carDetailsId'),)

    carId =models.ForeignKey(newCar,on_delete=models.CASCADE )
    carDetailsId =models.ForeignKey(carDetails_Bool,on_delete=models.CASCADE )
    value =models.BooleanField()

class publishedCar(models.Model):
    customerId =models.ForeignKey(User,on_delete=models.CASCADE )
    modelId = models.ForeignKey(model, on_delete=models.CASCADE)
    الفئة = models.CharField(max_length=20)
    السنة = models.IntegerField()
    السعر = models.FloatField(max_length=10)


class used_image(models.Model):
    carId =models.ForeignKey(publishedCar ,on_delete=models.CASCADE)
    صورة = models.ImageField(default='default.jpg', upload_to='car_img')

class publishedCarDetails_Text(models.Model):
    name =models.CharField(max_length=20 ,primary_key=True)

class publishedCarDetails_Bool(models.Model):
    name =models.CharField(max_length=20 ,primary_key=True)

class publishedCarContents_Text(models.Model):
    class Meta:
        unique_together =(('publishedCarId', 'publishedCarDetailsId'),)

    publishedCarId =models.ForeignKey(publishedCar,on_delete=models.CASCADE )
    publishedCarDetailsId =models.ForeignKey(publishedCarDetails_Text,on_delete=models.CASCADE )
    value =models.CharField(max_length=50)

class publishedCarContents_Bool(models.Model):
    class Meta:
        unique_together =(('publishedCarId', 'publishedCarDetailsId'),)

    publishedCarId =models.ForeignKey(publishedCar,on_delete=models.CASCADE )
    publishedCarDetailsId =models.ForeignKey(publishedCarDetails_Bool,on_delete=models.CASCADE )
    value =models.BooleanField()




from django.shortcuts import render,redirect
from django.views.generic import CreateView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .form import UserUpdateForm, ProfileUpdateForm ,governorate_form
from .models import region
from django.contrib.auth.decorators import login_required

class register(CreateView):
    template_name = 'user/register.html'
    form_class = UserCreationForm
    success_url = '/login'


def profile(request, pk):
    person = User.objects.get(id=pk)

    context = {
        'person': person,
    }

    return render(request, 'user/profile.html', context)

@login_required(login_url='/login/')
def profile_update(request):
    g_form = governorate_form(request.POST or None ,instance=request.user)
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            i =request.user.id
            return redirect('/profile/' +str(i))

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form,
        'g_form' :g_form
    }

    return render(request, 'user/profile_update.html', context)


#ajax
def load_regions(request):
    gov =request.GET.get('المحافظة')
    regions =region.objects.filter(governorateId = gov)

    return render(request ,'user/test_region.html' ,{'regions' :regions})

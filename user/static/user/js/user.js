
    $(function(){
    'use strict';
    /*==== slider ======*/
    $('.slider').bxSlider({
    mode: 'fade' ,
    pager:false,
    });

    /*==== scroll ======*/
    $(".scroll").niceScroll(".wrap" ,
    {
    cursorcolor:"#D72C1A",
    cursorwidth:"10px",
    cursorborder:"none",
    });



    /*==== loading ======*/
    /*********************/
	var counter = 0;
	function updateCounter(){
        /* === stop counter when reach to 100 ===*/
		if(counter == 10){
			clearInterval(foo);
			$('.loadingpage').addClass("pageisloaded");
		}
		else{
			$('.counter2 h1').html(counter);
			counter++;
		}
	}

	var foo = setInterval(updateCounter ,20);
    /*==== to top button ======*/

    // fade in if scroll > 2200
    $(window).scroll(function(){
        if ($(this).scrollTop() >= 500){
            $('.to-up').fadeIn(500);      // fade in if scroll > 2200
        }else {
            $('.to-up').fadeOut(500);     // fade Out if scroll < 2200
        }
    });
     /// when click
    $('.to-up').click(function(){
        $('html, body').animate({
            scrollTop:0                  /// scroll top to 0 or top
        },500);
    });
    ////////when scroll
    $(window).on('scroll',function(){
        $('.block').each(function(){
            if ($(window).scrollTop() > $(this).offset().top -6){
                var blockId = $(this).attr('id');
                $('.navbar-dark .navbar-nav .nav-link').removeClass('active');
                $(".navbar-dark .navbar-nav .nav-link[data-scroll=" + blockId +"").addClass('active')
            }
        })
    });
});





from django.shortcuts import render,redirect
from car.models import newCar ,company
from .form import search_form

def home(request):
    companies =company.objects.all()
    cars = newCar.objects.all()
    form = search_form(request.POST or None)

    context ={
        'companies' :companies ,
        'cars' :cars ,
        'form' :form
    }

    if request.method == 'POST':
        if form.is_valid():
            comp =form.cleaned_data['الماركة']
            model =form.cleaned_data['الموديل']
            type =form.cleaned_data['نوع_السيارة']


            if type == '1':
                return redirect('new_search', comp=comp ,model =model)

            elif type == '2':
                return redirect('used_search', comp=comp, model=model)




    return render(request ,'home/home.html' ,context)
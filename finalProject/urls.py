"""finalProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import  url
from car_prediction.api import CarAdList ,CarAdDetail
from rest_framework.urlpatterns import format_suffix_patterns




from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from user.views import register, profile,profile_update ,load_regions
from django.contrib.auth import views
from home.views import home
from search.views import used_search, new_search
from car_prediction.views import prediction ,userSubmit,chrome ,home2
from car.views import add_used_car ,used_car,new_car ,load_models ,add_new_car ,update_new_car \
      ,update_used_car ,delete_new_car,delete_used_car

urlpatterns = [
      path('admin/', admin.site.urls),
      # home app____________________________________________________________________________
      path('', home, name='home'),
      # user app______________________________________________________________________
      path('register/', register.as_view(), name='register'),
      path('login/', views.LoginView.as_view(template_name='user/login.html'), name='login'),
      path('logout/', views.LogoutView.as_view(template_name='user/logout.html'), name='logout'),
      path('profile/<int:pk>/',profile, name='profile'),
      path('profile/update/',profile_update, name='profile_update'),

      # search app____________________________________________________________
      path('used_search/<str:comp>/<str:model>/', used_search.as_view(), name='used_search'),
      path('new_search/<str:comp>/<str:model>/', new_search.as_view(), name='new_search'),
      # car app_______________________________________________________
      path('used_car/<int:pk>/', used_car.as_view(), name='used_car'),
      path('new_car/<int:pk>/',new_car.as_view(), name='new_car'),

      path('add_used_car/', add_used_car, name='add_used_car'),
      path('add_new_car/', add_new_car, name='add_new_car'),

      path('update_new_car/<int:pk>/', update_new_car, name='update_new_car'),
      path('update_used_car/<int:pk>/', update_used_car, name='update_used_car'),

      path('delete_new_car/<int:pk>/', delete_new_car.as_view(), name='delete_new_car'),
      path('delete_used_car/<int:pk>/', delete_used_car.as_view(), name='delete_used_car'),
      # ajax__________________________________________________________
      path('ajax/load_models/' ,load_models ,name ='ajax_load_models' ),
      path('ajax/load_regions/' ,load_regions ,name ='ajax_load_regions' ),

      # prediction__________________________________________________________________
      path('prediction/', prediction, name='prediction'),
                    url(r'^x$', home2, name='home2'),

                    url(r'^userSubmit',userSubmit, name="userSubmit"),

                    url(r'^chrome',chrome, name="chrome"),

                    url(r'^api/ads/$',CarAdList.as_view()),
                    url(r'^api/ads/(?P<idFromSite>[0-9]+)/$', CarAdDetail.as_view())

  ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns = format_suffix_patterns(urlpatterns)
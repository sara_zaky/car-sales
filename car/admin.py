from django.contrib import admin
from.models import *


admin.site.register(company)
admin.site.register(model)
admin.site.register(newCar)
admin.site.register(publishedCar)
admin.site.register(carDetails_Text)
admin.site.register(carDetails_Bool)
admin.site.register(carContents_Text)
admin.site.register(carContents_Bool)
admin.site.register(publishedCarDetails_Text)
admin.site.register(publishedCarDetails_Bool)
admin.site.register(publishedCarContents_Text)
admin.site.register(publishedCarContents_Bool)
admin.site.register(image)
admin.site.register(used_image)

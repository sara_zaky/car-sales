from django.shortcuts import render
from django.views.generic import ListView
from car.models import publishedCar ,company ,model ,newCar

class used_search(ListView):
    model = publishedCar
    template_name = 'search/used_search.html'

    def get_queryset(self):
        x = self.kwargs['comp']
        y = self.kwargs['model']

        dic = {}
        if x == 'None':
            comp_list = company.objects.all()
        else:
            comp_list = company.objects.filter(الماركة=x)

        for i in comp_list:
            s = i.الماركة

            if y == 'None':
                model_list = model.objects.filter(companyId=i)
            else:
                model_list = model.objects.filter(الموديل=y)

            dic2 = {}
            for j in model_list:
                s_model = j.الموديل
                car_list = publishedCar.objects.filter(modelId = j)
                dic2[s_model] = car_list

            dic[s] = dic2

        return dic


class new_search(ListView):
    model = newCar
    template_name = 'search/new_search.html'

    def get_queryset(self):
        x = self.kwargs['comp']
        y =self.kwargs['model']

        dic = {}
        if x == 'None' :
            comp_list = company.objects.all()
        else:
            comp_list = company.objects.filter(الماركة=x)

        for i in comp_list:
            s = i.الماركة

            if y=='None' :
                model_list = model.objects.filter(companyId=i)
            else:
                model_list = model.objects.filter(الموديل=y)

            dic2 = {}
            for j in model_list:
                s_model = j.الموديل
                car_list = newCar.objects.filter(modelId=j)
                dic2[s_model] = car_list

            dic[s] = dic2

        return dic

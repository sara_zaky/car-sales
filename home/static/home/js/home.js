 $(function(){
    'use strict';
    /*==== portfolio ======*/


        $('.portfolio .box').hover(function(){
        $(this).children('i').css({
            backgroundColor:'#ec1c23', // change bg color to fa-search
        });
    },function(){
        $(this).children('i').css({
            backgroundColor:'#000',
        });
    });
      //  custmoize tabs
    $('.portfolio li').click(function(){
        $(this).addClass('active2').siblings().removeClass('active2');
        if ($(this).data('class') == 'all'){
            $('.portfolio .box').fadeIn(200)
        }else {
            $('.portfolio .box').fadeOut(200);
            $($(this).data('class')).fadeIn(200)
        }
    });


    function mediaSize() {


   if (window.matchMedia('(max-width:768px)').matches){
    $('.review .slideres').slick({

        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: true,
        swipeToSlide: false,
        accessibility: false,
        prevArrow:".prev",
        nextArrow:".next"

    });
   }else {
    $('.review .slideres').slick({

        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: true,
        swipeToSlide: false,
        accessibility: false,
        prevArrow:".prev",
        nextArrow:".next"

    });

   }
}
mediaSize();
window.addEventListener('resize',mediaSize,true);
});
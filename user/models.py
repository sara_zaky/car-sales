from django.db import models
from django.contrib.auth.models import User

class governorate(models.Model):
    المحافظة =models.CharField(max_length=30)

    def __str__(self):
        return self.المحافظة


class region(models.Model):
    governorateId =models.ForeignKey(governorate ,on_delete=models.CASCADE)
    المنطقة =models.CharField(max_length=30)

    def __str__(self):
        return self.المنطقة



class profile(models.Model):
    user = models.OneToOneField(User , on_delete=models.CASCADE)
    الاسم = models.CharField( max_length=20  ,default="-")
    صورة_شخصية = models.ImageField(default='default.png' , upload_to='profile_img')
    رقم_الهاتف = models.CharField( max_length=13,default="-")
    المنطقة =models.ForeignKey(region ,on_delete=models.CASCADE ,null=True ,blank=True)


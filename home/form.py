from django import forms
from car.models import company ,model ,publishedCar

class search_form(forms.Form):
    الماركة = forms.ModelChoiceField(queryset=company.objects.all(),required=False)
    الموديل = forms.ModelChoiceField(queryset=model.objects.none() ,required=False)
    query =(
        (1,'سيارة جديدة'),
        (2,'سيارة مستعملة')
    )

    نوع_السيارة =forms.ChoiceField(choices=query)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['الموديل'].queryset = model.objects.none()

        if 'الماركة' in self.data:
            try:
                comp_id = int(self.data.get('الماركة'))
                self.fields['الموديل'].queryset = model.objects.filter(companyId=comp_id)
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset

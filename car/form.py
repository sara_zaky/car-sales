from django import forms
from django.forms.formsets import formset_factory
from .models import *




class comp_form(forms.ModelForm):
    الماركة = forms.ModelChoiceField(queryset= company.objects.all() )

    class Meta:
        model =company
        fields =['الماركة',]

    def __init__(self, *args, **kwargs):
        super(comp_form, self).__init__(*args, **kwargs)

        if self.instance.id:
            self.initial['الماركة'] = self.instance.id
#_________________________________________________________________________________________


#used car
class used_car_form_txt(forms.ModelForm):
    class Meta:
        model = publishedCarContents_Text
        fields =()

    def __init__(self, *args, **kwargs):
        super(used_car_form_txt, self).__init__(*args, **kwargs)

        if self.instance.pk:
            details = publishedCarContents_Text.objects.filter(publishedCarId=self.instance)
            for d in details:
                self.fields['%s' % d.publishedCarDetailsId.name] = forms.CharField(label=d.publishedCarDetailsId.name,max_length=100 , initial=d.value)
        else:
            for d in publishedCarDetails_Text.objects.all():
                self.fields['%s' % d.name] = forms.CharField(max_length=100, label=d.name)

#_________________________________________________________________________________________

class used_car_form_bool(forms.ModelForm):
    class Meta:
        model = publishedCarContents_Bool
        fields =()

    def __init__(self, *args, **kwargs):
        super(used_car_form_bool, self).__init__(*args, **kwargs)

        if self.instance.pk:
            details = publishedCarContents_Bool.objects.filter(publishedCarId=self.instance)
            for d in details:
                self.fields['%s' % d.publishedCarDetailsId.name] = forms.BooleanField(label=d.publishedCarDetailsId.name,required=False , initial=d.value)
        else:
            for d in publishedCarDetails_Bool.objects.all() :
                self.fields['%s' % d.name] = forms.BooleanField(label=d.name ,required=False)
#_________________________________________________________________________________________



class publishedCar_form(forms.ModelForm):
    class Meta:
        model = publishedCar
        fields = ('modelId', 'الفئة', 'السنة', 'السعر')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['modelId'].queryset = model.objects.none()
        self.fields['modelId'].label = 'الموديل'



        if 'الماركة' in self.data:
            try:
                comp_id = int(self.data.get('الماركة'))
                self.fields['modelId'].queryset = model.objects.filter(companyId=comp_id)
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['modelId'].queryset = self.instance.modelId.companyId.model_set.order_by('الموديل')
#_________________________________________________________________________________________
class used_img_form(forms.ModelForm):
    class Meta:
        model =used_image
        fields =('صورة',)

    def __init__(self, *args, **kwargs):
        super(used_img_form, self).__init__(*args, **kwargs)
        self.fields['صورة'].required = False

used_img_formset = forms.modelformset_factory(used_image,form =used_img_form, extra=10 ,max_num =10)

#_________________________________________________________________________________________


#new car
class new_car_form_txt(forms.ModelForm):
    class Meta:
        model = carContents_Text
        fields =()

    def __init__(self, *args, **kwargs):
        super(new_car_form_txt, self).__init__(*args, **kwargs)

        if self.instance.pk:
            details = carContents_Text.objects.filter(carId=self.instance)
            for d in details:
                self.fields['%s' % d.carDetailsId.name] = forms.CharField(label=d.carDetailsId.name,max_length=100 , initial=d.value)
        else:
            for d in carDetails_Text.objects.all():
                self.fields['%s' % d.name] = forms.CharField(max_length=100,label=d.name)


#_________________________________________________________________________________________

class new_car_form_bool(forms.ModelForm):
    class Meta:
        model = carContents_Bool
        fields =()

    def __init__(self, *args, **kwargs):
        super(new_car_form_bool, self).__init__(*args, **kwargs)

        if self.instance.pk:
            details = carContents_Bool.objects.filter(carId=self.instance)
            for d in details:
                self.fields['%s' % d.carDetailsId.name] = forms.BooleanField(label=d.carDetailsId.name, required=False,
                                                                             initial=d.value)
        else:
            for d in carDetails_Bool.objects.all():
                self.fields['%s' % d.name] = forms.BooleanField(label=d.name, required=False)
#_________________________________________________________________________________________

class car_form(forms.ModelForm):
    class Meta:
        model = newCar
        fields = ('modelId', 'الفئة', 'السنة', 'السعر')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['modelId'].queryset = model.objects.none()
        self.fields['modelId'].label = 'الموديل'

        if 'الماركة' in self.data:
            try:
                comp_id = int(self.data.get('الماركة'))
                self.fields['modelId'].queryset = model.objects.filter(companyId=comp_id)
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['modelId'].queryset = self.instance.modelId.companyId.model_set.order_by('الموديل')

#_________________________________________________________________________________________
class new_img_form(forms.ModelForm):
    class Meta:
        model =image
        fields =('صورة',)

    def __init__(self, *args, **kwargs):
        super(new_img_form, self).__init__(*args, **kwargs)
        self.fields['صورة'].required = False

new_img_formset = forms.modelformset_factory(image ,form =new_img_form, extra=10 ,max_num=10)
#_________________________________________________________________________________________


from django.shortcuts import render ,redirect,get_object_or_404
from .models import *

from .form import used_car_form_txt ,used_car_form_bool ,car_form,\
    comp_form,publishedCar_form ,new_car_form_bool ,new_car_form_txt ,used_img_formset ,new_img_formset

from django.views.generic import DetailView ,DeleteView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import  staff_member_required
from django.contrib.auth.mixins import LoginRequiredMixin


#used car
class used_car(DetailView):
    model = publishedCar
    template_name = 'car/used_car.html'



#_______________________________________________________________________________________________
class delete_used_car(LoginRequiredMixin,DeleteView):
    model =publishedCar
    success_url = '/'
    template_name =  'car/delete_car.html'

    def delete(self, request, *args, **kwargs):
        object =self.get_object()

        if  object.customerId ==self.request.user or self.request.user.is_superuser:
            id =object.customerId.id
            object.delete()
            return redirect('profile' ,pk =id)

        return redirect('used_car', pk=object.id)


# _______________________________________________________________________________________________


@login_required(login_url='/login/')
def add_used_car(request):
    form_txt = used_car_form_txt(request.POST or None)
    form_bool = used_car_form_bool(request.POST or None)
    pub_form = publishedCar_form(request.POST or None)
    com_form = comp_form(request.POST or None)
    img_form = used_img_formset(request.POST or None ,request.FILES or None,queryset =image.objects.none())

    context = {'form_txt': form_txt,
               'form_bool': form_bool,
               'com_form' :com_form,
               'pub_form' :pub_form,
               'img_form' :img_form,
               'used' :1,
               }


    if request.method == 'POST':
        if form_bool.is_valid() and form_txt.is_valid() and pub_form.is_valid() and img_form.is_valid():
            car_ins = pub_form.save(commit=False)
            car_ins.customerId = request.user
            car_ins.save()

            for i, j in form_bool.cleaned_data.items() :
                d =publishedCarDetails_Bool.objects.get(name = i)
                publishedCarContents_Bool.objects.create(publishedCarId = car_ins,
                                                         publishedCarDetailsId =d,
                                                         value =j
                                                         )

            for i, j in form_txt.cleaned_data.items() :
                d =publishedCarDetails_Text.objects.get(name = i)
                publishedCarContents_Text.objects.create(publishedCarId = car_ins,
                                                         publishedCarDetailsId =d,
                                                         value =j
                                                         )

            for img in img_form:
                if img.cleaned_data:
                    img_ins =img.save(commit=False)
                    img_ins.carId =car_ins
                    img_ins.save()

            return redirect('used_car' ,pk =car_ins.id)

    return render(request , 'car/add_car.html' ,context)


#_______________________________________________________________________________________________


@login_required(login_url='/login/')
def update_used_car(request ,pk):
    car_ins =get_object_or_404(publishedCar ,id=pk)

    if request.user == car_ins.customerId:
        form_txt = used_car_form_txt(request.POST or None, instance=car_ins)
        form_bool = used_car_form_bool(request.POST or None, instance=car_ins)
        pub_form = publishedCar_form(request.POST or None, instance=car_ins)
        com_form = comp_form(request.POST or None, instance=car_ins.modelId.companyId)
        img_form = used_img_formset(request.POST or None, request.FILES or None
                                    ,queryset=used_image.objects.filter(carId =car_ins))

        context = {'form_txt': form_txt,
                   'form_bool': form_bool,
                   'com_form' :com_form,
                   'pub_form' :pub_form,
                   'img_form': img_form,
                   'pku': pk
                   }


        if request.method == 'POST':
            if form_bool.is_valid() and form_txt.is_valid() and pub_form.is_valid()and img_form.is_valid():
                car_ins = pub_form.save()

                contents_txt = publishedCarContents_Text.objects.filter(publishedCarId=car_ins)
                contents_bool = publishedCarContents_Bool.objects.filter(publishedCarId=car_ins)

                for i, j in form_bool.cleaned_data.items():
                    d = publishedCarDetails_Bool.objects.get(name=i)
                    c = contents_bool.get(publishedCarDetailsId=d)
                    c.value = j
                    c.save()

                for i, j in form_txt.cleaned_data.items():
                    d = publishedCarDetails_Text.objects.get(name=i)
                    c = contents_txt.get(publishedCarDetailsId=d)
                    c.value = j
                    c.save()

                    for img in img_form:
                        if img.cleaned_data:
                            img_ins = img.save(commit=False)
                            img_ins.carId = car_ins
                            img_ins.save()

                return redirect('used_car' ,pk =car_ins.id)

        return render(request , 'car/add_car.html' ,context)

    else:
        return redirect('used_car', pk=car_ins.id)


#_______________________________________________________________________________________________


#new car
class new_car(DetailView):
    model =newCar
    template_name = 'car/new_car.html'

#_______________________________________________________________________________________________
class delete_new_car(DeleteView):
    model = newCar
    success_url = '/'
    template_name = 'car/delete_car.html'

    def delete(self, request, *args, **kwargs):
        object =self.get_object()

        if  self.request.user.is_superuser :
            object.delete()
            return redirect('home')

        return redirect('new_car', pk=object.id)


# _______________________________________________________________________________________________

@staff_member_required
def add_new_car(request):
    form_txt = new_car_form_txt(request.POST or None)
    form_bool = new_car_form_bool(request.POST or None)
    pub_form = car_form(request.POST or None)
    com_form = comp_form(request.POST or None)
    img_form = new_img_formset(request.POST or None, request.FILES or None ,queryset =image.objects.none())


    context = {'form_txt': form_txt,
               'form_bool': form_bool,
               'com_form' :com_form,
               'pub_form' :pub_form,
               'img_form': img_form,
               }


    if request.method == 'POST':
        if form_bool.is_valid() and form_txt.is_valid() and pub_form.is_valid() and img_form.is_valid():
            car_ins = pub_form.save()

            for i, j in form_bool.cleaned_data.items() :
                d =carDetails_Bool.objects.get(name = i)
                carContents_Bool.objects.create(carId = car_ins,
                                                carDetailsId =d,
                                                value =j
                                                )

            for i, j in form_txt.cleaned_data.items() :
                d =carDetails_Text.objects.get(name = i)
                carContents_Text.objects.create(carId = car_ins,
                                                carDetailsId =d,
                                                value =j
                                                )

            for img in img_form:
                if img.cleaned_data:
                    img_ins =img.save(commit=False)
                    img_ins.carID =car_ins
                    img_ins.save()
        return redirect('new_car' ,pk =car_ins.id)

    return render(request , 'car/add_car.html' ,context)


#_______________________________________________________________________________________________


@staff_member_required
def update_new_car(request ,pk):
    car_ins = get_object_or_404(newCar, id=pk)

    pub_form = car_form(request.POST or None, instance=car_ins)
    com_form = comp_form(request.POST or None, instance=car_ins.modelId.companyId)
    form_bool = new_car_form_bool(request.POST or None, instance=car_ins)
    form_txt = new_car_form_txt(request.POST or None, instance=car_ins)
    img_form = new_img_formset(request.POST or None, request.FILES or None,queryset=image.objects.filter(carID=car_ins))

    context = {
        'pub_form': pub_form,
        'com_form': com_form,
        'form_bool': form_bool,
        'form_txt': form_txt,
        'img_form': img_form,
        'pkn': pk
    }
    if request.method == 'POST':
        if form_bool.is_valid() and form_txt.is_valid() and pub_form.is_valid()and img_form.is_valid():
            car_ins = pub_form.save()

            contents_txt = carContents_Text.objects.filter(carId=car_ins)
            contents_bool = carContents_Bool.objects.filter(carId=car_ins)

            for i, j in form_bool.cleaned_data.items() :
                d = carDetails_Bool.objects.get(name=i)
                c=contents_bool.get(carDetailsId =d)
                c.value =j
                c.save()

            for i, j in form_txt.cleaned_data.items() :
                d =carDetails_Text.objects.get(name = i)
                c = contents_txt.get(carDetailsId=d)
                c.value = j
                c.save()

            for img in img_form:
                if img.cleaned_data:
                    img_ins =img.save(commit=False)
                    img_ins.carID =car_ins
                    img_ins.save()

        return redirect('new_car', pk=car_ins.id)

    return render(request, 'car/add_car.html', context)

#_______________________________________________________________________________________________
#ajax
def load_models(request):
    comp =request.GET.get('الماركة')
    models =model.objects.filter(companyId = comp)

    return render(request ,'car/test_model.html' ,{'models' :models})
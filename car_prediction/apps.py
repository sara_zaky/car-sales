from django.apps import AppConfig


class CarPredictionConfig(AppConfig):
    name = 'car_prediction'

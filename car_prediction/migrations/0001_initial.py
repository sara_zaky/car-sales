# Generated by Django 2.1.5 on 2019-06-15 21:27

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CarAd',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kw', models.CharField(max_length=20)),
                ('km', models.CharField(max_length=20)),
                ('year', models.CharField(max_length=20)),
                ('ac', models.CharField(max_length=20)),
                ('gears', models.CharField(max_length=20)),
                ('body', models.CharField(max_length=20)),
                ('price', models.CharField(max_length=20)),
                ('idFromSite', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='CarAd1',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('power', models.IntegerField()),
                ('mileage', models.IntegerField()),
                ('year', models.IntegerField()),
                ('airCondition', models.CharField(max_length=30)),
                ('gearBox', models.CharField(max_length=20)),
                ('chassis', models.CharField(max_length=20)),
                ('price', models.IntegerField()),
                ('idFromSite', models.IntegerField()),
                ('fuelType', models.CharField(max_length=20)),
            ],
        ),
    ]
